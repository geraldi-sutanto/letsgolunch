const choices = ["yong tau fu", "middle", "wong kok", "malay", "indo", "mamak"];
const port = process.env.PORT || 8080;

console.log(`listening on port ${port}`);

require("http")
  .createServer((req, res) => {
    res.write(choices[~~(Math.random() * choices.length)]);
    res.end();
  })
  .listen(port);
